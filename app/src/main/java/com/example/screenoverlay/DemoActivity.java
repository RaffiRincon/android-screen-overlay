package com.example.screenoverlay;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Messenger;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import static android.view.WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;

public class DemoActivity extends AppCompatActivity {

    private static final int JOB_ID = 9654;
    Messenger mService = null;
    boolean bound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);


    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = new Messenger(service);
            bound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            bound = false;
        }
    };

    public void presentOverlay(View v) {
        Intent i = new Intent(DemoActivity.this, OverlayService.class);
        startService(i);
    }
//
//    class AddTask extends AsyncTask<Void, Void, Void> {
//
//        private void add() {
//            WindowManager windowManager = (WindowManager) getBaseContext().getSystemService(Context.WINDOW_SERVICE);
//            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.FIRST_SUB_WINDOW);
//            layoutParams.width = 300;
//            layoutParams.height = 300;
//            layoutParams.x = 0;
//            layoutParams.y = 0;
//
//            layoutParams.format = PixelFormat.RGBA_8888;
//            layoutParams.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | FLAG_HARDWARE_ACCELERATED;
//            layoutParams.token = getWindow().getDecorView().getRootView().getWindowToken();
//            layoutParams.setTitle("Overlay");
//
//            //Feel free to inflate here
//            View mTestView = new View(getApplicationContext());
//            mTestView.setBackgroundColor(Color.RED);
//
//            windowManager.addView(mTestView, layoutParams);
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//            try {
//                Thread.sleep(1500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                add();
//            }
//
//            add();
//            return  null;
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}